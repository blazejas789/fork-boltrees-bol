--[[
	Avoid Interrupts by Trees
    1.0     - initial release
	TODO: Add support for champions with skills that channel. Finish dom cap support.
	Description: This script will attempt to block interruptions while doing certain things.
]]

local recall = false
local cap = false
local rightClick = false

function OnLoad()
	Menu = scriptConfig('Interrupt Avoider', 'IA.cfg')
	Menu:addParam('recall', 'On Recall', SCRIPT_PARAM_ONOFF, true)
	--Menu:addParam('cap', 'On Capture', SCRIPT_PARAM_ONOFF, true)
	Menu:addParam('spell', 'Block Spellcast', SCRIPT_PARAM_ONOFF, true)
	Menu:addParam('move', 'Block Movement', SCRIPT_PARAM_ONOFF, true)
end

function OnRecall(unit)
	if unit and unit.valid and unit.isMe and Menu.recall then recall = true end
end

function OnAbortRecall(unit)
	if unit and unit.valid and unit.isMe and Menu.recall then recall = false end
end

function OnFinishRecall(unit)
	if unit and unit.valid and unit.isMe and Menu.recall then recall = false end
end

function OnSendPacket(p)
	-- or (Menu.cap and cap)
	if ((p.header == Packet.headers.S_MOVE and Menu.move and not rightClick and not _G.Evade) or (p.header == Packet.headers.S_CAST and Menu.spell)) and ((Menu.recall and recall)) then p:Block() end
end

function OnWndMsg(msg, key)
	if msg == WM_RBUTTONDOWN then rightClick = true
	elseif msg == WM_RBUTTONUP then rightClick = false
	end
end