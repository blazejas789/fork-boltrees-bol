class 'OnJungleBuff' -- {

function OnJungleBuff:__init()
	-- buffNames for new callbacks
	local buffNames = { 'crestoftheancientgolem' , 'blessingofthelizardelder', 'exaltedwithbaronnashor' }
	-- callBacks
	local callBacks = { 
								{'OnGainBlueBuff', 'OnLoseBlueBuff', 'OnUpdateBlueBuff'}
								{'OnGainRedBuff', 'OnLoseRedBuff', 'OnUpdateRedBuff'}
								{'OnGainBaronBuff', 'OnLoseBaronBuff', 'OnUpdateBaronBuff'} 
							}
							
	-- register callBacks
		for i,v in callBacks do
			_ENV = AdvancedCallback:register(table.unpack(callBacks[i]))
		end
		
	-- bind buff callbacks to buffHandler
	AdvancedCallback:bind('OnGainBuff', function(unit, buff) self:buffHandler(unit, buff, 1) return end)
	AdvancedCallback:bind('OnLoseBuff', function(unit, buff) self:buffHandler(unit, buff, 2) return end)
	AdvancedCallback:bind('OnUpdateBuff', function(unit, buff) self:buffHandler(unit, buff, 3) return end)
end

function OnJungleBuff:buffHandler(unit, buff, gain)
	if unit and unit.valid then
		--cycle through buffNames to determine the buff
		for i,v in ipairs(buffNames) do 
			if buff.name == v then	AdvancedCallback[callBacks[i][gain]](AdvancedCallback, unit, buff) break end
		end
	end
end
-- }