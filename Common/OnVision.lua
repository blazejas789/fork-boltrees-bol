if VIP_USER then
	-- callbacks
	local callBacks = { 'OnLoseVision', 'OnGainVision', 'OnHideUnit', 'OnShowUnit' }
	
	-- register new callbacks
	_ENV = AdvancedCallback:register(table.unpack(callBacks))
	
	-- packet headers
	local pHeaders = { Packet.headers.PKT_S2C_LoseVision, Packet.headers.PKT_S2C_GainVision, Packet.headers.PKT_S2C_HideUnit, Packet.headers.R_WAYPOINT }

	function OnRecvPacket(p)
		-- cycle through pHeaders to match packet header
		for index, header in ipairs(pHeaders) do
			if p.header == header then
				local unit = objManager:GetObjectByNetworkId(Packet(p):get('networkId'))
				if unit and unit.valid then
					if AdvancedCallback[callBacks[i]](AdvancedCallback, unit) == false then -- client side blocking
						p:Block()
						return
					end
				end
			end
		end
	end
	
end