_ENV = AdvancedCallback:register('BeforeAttack', 'BeforeBOLAttack', 'OnSendAttack', 'OnRecvAttack','OnSelfDeath')

_G.myHero.Attack2 = _G.myHero.Attack
_G.myHero.Attack = function(self, target)
	if target and target.valid then
		if AdvancedCallback:BeforeBOLAttack(target) == false then return end
		_G.myHero.Attack2(self, target)
	end
end

function OnSendPacket(p)
	if p.header ~= Packet.headers.S_MOVE then return end
	local decodedPacket = Packet(p)
	local target = objManager:GetObjectByNetworkId(decodedPacket:get('targetNetworkId'))
	local type = decodedPacket:get('type')
	if type ~= nil and (type == 3 or type == 7) and target ~= nil and AdvancedCallback:BeforeAttack(target) == false then p:Block() end
end

function OnAttack(unit, target)
	if unit and unit.valid then
		if unit.isMe then
			if AdvancedCallback:OnSendAttack(target) == false then return false end
		elseif target.isMe then 
			if AdvancedCallback:OnRecvAttack(unit) == false return false end
		end
	end
end

function OnHeroKilled(killed, killer)
	if killed and killed.valid and killed.isMe then AdvancedCallback:OnSelfDeath(killer) end
end